#
# --- base image---
#
FROM ubuntu:18.04
ENV DEBIAN_FRONTEND nonineractive

LABEL maintainer "hanakamu <hanakamu321@gmail.com>"

#
# -- build tools --
#
RUN apt update && apt upgrade -y

RUN apt install python3 -y
RUN apt install python3-pip -y
RUN apt install python3-dev -y
RUN pip3 install --upgrade pip

RUN apt install libopus-dev -y
RUN apt install libvpx-dev -y
RUN apt install libffi-dev -y
RUN apt install libssl-dev -y
RUN apt install libopencv-dev -y

RUN apt install libavdevice-dev -y
RUN apt install libavfilter-dev -y 
RUN apt install pkg-config -y

#
# -- aiortc --
#
RUN apt install git -y

RUN mkdir /root/work 
WORKDIR /root/work/
RUN git clone https://github.com/jlaine/aiortc.git

RUN pip install aiohttp
RUN pip install aiortc 
RUN pip install opencv-python

#
# ------ yolo v3 ---
#

RUN apt install wget -y

WORKDIR /root/work/
RUN git clone https://gitlab.com/hanakamu/webrtc-yolov3.git

RUN cp /root/work/webrtc-yolov3/volume/server.py /root/work/aiortc/examples/server/
RUN cp /root/work/webrtc-yolov3/volume/index_w.html /root/work/aiortc/examples/server/
RUN cp /root/work/webrtc-yolov3/volume/client_w.js /root/work/aiortc/examples/server/
RUN cp /root/work/webrtc-yolov3/volume/yolo_utils.py /root/work/aiortc/examples/server/
RUN cp -r /root/work/webrtc-yolov3/volume/yolov3-coco /root/work/aiortc/examples/server/

WORKDIR /root/work/aiortc/examples/server/yolov3-coco
RUN wget https://pjreddie.com/media/files/yolov3.weights

#
# --- AppDynamics --
#
RUN pip install -U appdynamics

# --- for running --
EXPOSE 8080

WORKDIR /root/work/aiortc/examples/server/
CMD [ "python3", "server.py" ]