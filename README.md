# webrtc-yolov3

This container based application provides object detection on your browser via webRTC.

## Usage

### Docker hub

1. Pull an image from docker hub & Run this app.

```
docker run -d -p 30080:8080 hanakm/webrtc-yolov3:<version>
```

2. [localhost:30080](http://localhost:30080) : Open this page via your browser. (Recommended: Chrome)
3. When you click a "Start" button.
4. If you get an alert, please click "Allow".
5. Browser will connect to the app server on localhost via WebRTC.
6. If you want to restart it, please click "Stop" button and reload this page.

### Local build

1. Clone this project via git.

```
git clone https://gitlab.com/hanakamu/webrtc-yolov3.git
```

2. Move to project's directory.

```
cd webrtc-yolov3
```

3. Build a docker image using a Dockerfile.
```
docker build -t webrtc-yolov3 -f Dockerfile .
```

4. Run this local image.

```
docker run -d -p 30080:8080 webrtc-yolov3
```

5. Access the app page in ths same way as case of using docker hub.


## k8s support

- under testing...


## Reference
[mganeko/aiortc_yolov3](https://github.com/mganeko/aiortc_yolov3)

[YOLOv3-Object-Detection-with-OpenCV](https://github.com/iArunava/YOLOv3-Object-Detection-with-OpenCV)

