import argparse
import asyncio
import json
import logging
import math
import os
import time
import wave

import cv2 as cv
import numpy as np
from aiohttp import web

from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.mediastreams import (AudioFrame, AudioStreamTrack, VideoFrame,
                                 VideoStreamTrack)

# import yolo_utils.py
from yolo_utils import infer_image, show_image, draw_labels_and_boxes

# --- multi process --
from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor
from multiprocessing import Value, Array, RawArray

# --- AppDynamics --
from appdynamics.agent import api as appd
appd.init()

BT_NAME_DETECT = 'Object Detection'


ROOT = os.path.dirname(__file__)

# --- status of worker process --
executor = None
#workerBusy = False
lastDetects = None

workerBusy = Value('i', 0)

# --- detect with YOLO v3 ---
# -- detect in worker --
class DETECT_RESULT():
    def __init__(self, img, boxes, confidences, classids, idxs):
        self.img = img
        self.boxes = boxes
        self.confidences = confidences
        self.classids = classids
        self.idxs = idxs

class AudioFileTrack(AudioStreamTrack):
    def __init__(self, path):
        self.last = None
        self.reader = wave.open(path, 'rb')
        self.frames_per_packet = int(self.reader.getframerate() * AUDIO_PTIME)

    async def recv(self):
        # as we are reading audio from a file and not using a "live" source,
        # we need to control the rate at which audio is sent
        if self.last:
            now = time.time()
            await asyncio.sleep(self.last + AUDIO_PTIME - now)
        self.last = time.time()

        return AudioFrame(
            channels=self.reader.getnchannels(),
            data=self.reader.readframes(self.frames_per_packet),
            sample_rate=self.reader.getframerate())


class VideoTransformTrack(VideoStreamTrack):
    def __init__(self, track, transform):
        super().__init__()  # don't forget this!
        self.counter = 0
        self.track = track
        self.received = asyncio.Queue(maxsize=1)
        self.transform = transform
        self.tryDetectCounter = 0

    async def recv(self):
        frame = await self.received.get()
        #frame = await self.track.recv()

        self.counter += 1

        # --- try reduce frame -- NG
        #if (self.counter % 2) == 1:
        #    return None

        if self.transform == 'yolov3_worker':
            with workerBusy.get_lock():
                if (workerBusy.value == 0):
                    # --- kick detect --
                    print('kick')
                    workerBusy.value = 1
                    img = frame.to_ndarray(format="bgr24")
                    future = executor.submit(detect, img)
                    future.add_done_callback(onDetected)
                    self.tryDetectCounter += 1
            
            img = frame.to_ndarray(format="bgr24")

            # -- draw boxes
            if(lastDetects!=None):
                print('draw boxes')
                img = draw_labels_and_boxes(img, lastDetects.boxes, lastDetects.confidences, lastDetects.classids, lastDetects.idxs, colors, labels)
            else :
                print('not Detect')
            new_frame = VideoFrame.from_ndarray(img, format="bgr24")
            new_frame.pts = frame.pts
            new_frame.time_base = frame.time_base
            #img = draw_labels_and_boxes(img, boxes, confidences, classids, idxs, colors, labels)
            # else:
            return new_frame
        else:
            # return raw frame
            return frame

# using yolo_utils
def detect(frame):

    count = 0
    height, width = frame.shape[:2]

    print(height, width)

    FINANCIALS_ID_PROPS = {'Host': 'financials-lb', 'Port': 3456, 'Vendor': 'custom db'}

    with appd.bt(BT_NAME_DETECT) as bt_handle:
        with appd.exit_call(bt_handle, appd, 'Financials Database', FINANCIALS_ID_PROPS):
        
            if count == 0:
                frame, boxes, confidences, classids, idxs = infer_image(net, layer_names, \
                                    height, width, frame, colors, labels, FLAGS)
                count += 1
            else:
                frame, boxes, confidences, classids, idxs = infer_image(net, layer_names, \
                                    height, width, frame, colors, labels, FLAGS, boxes, confidences, classids, idxs, infer=False)
                count = (count + 1) % 6
        
        
            result = DETECT_RESULT(frame, boxes, confidences, classids, idxs)
            #return frame
            return result

def onDetected(future):
    print('--- future on end---')
    #-- get result --
    global lastDetects
    lastDetects = future.result() # <--- wait at here

    print(lastDetects)

    # -- worker finish --
    with workerBusy.get_lock():
      workerBusy.value = 0

    return

async def consume_audio(track):
    """
    Drain incoming audio and write it to a file.
    """
    writer = None

    try:
        while True:
            frame = await track.recv()
            if writer is None:
                writer = wave.open(AUDIO_OUTPUT_PATH, 'wb')
                writer.setnchannels(frame.channels)
                writer.setframerate(frame.sample_rate)
                writer.setsampwidth(frame.sample_width)
            writer.writeframes(frame.data)
    finally:
        if writer is not None:
            writer.close()


async def consume_video(track, local_video):
    """
    Drain incoming video, and echo it back.
    """
    while True:
        frame = await track.recv()

        # we are only interested in the latest frame
        if local_video.received.full():
            await local_video.received.get()

        await local_video.received.put(frame)


async def index(request):
    content = open(os.path.join(ROOT, 'index_w.html'), 'r').read()
    return web.Response(content_type='text/html', text=content)


async def javascript(request):
    content = open(os.path.join(ROOT, 'client_w.js'), 'r').read()
    return web.Response(content_type='application/javascript', text=content)


async def offer(request):
    params = await request.json()
    offer = RTCSessionDescription(
        sdp=params['sdp'],
        type=params['type'])

    pc = RTCPeerConnection()
    pc._consumers = []
    pcs.append(pc)

    # prepare local media
    #local_audio = AudioFileTrack(path=os.path.join(ROOT, 'demo-instruct.wav'))
    #local_video = VideoTransformTrack(transform=params['video_transform'])

    @pc.on('datachannel')
    def on_datachannel(channel):
        @channel.on('message')
        def on_message(message):
            channel.send('pong')

    @pc.on('track')
    def on_track(track):
        if track.kind == 'audio':
            #pc.addTrack(local_audio)
            pc._consumers.append(asyncio.ensure_future(consume_audio(track)))
        elif track.kind == 'video':
            local_video = VideoTransformTrack(track, transform=params['video_transform'])
            pc.addTrack(local_video)
            pc._consumers.append(asyncio.ensure_future(consume_video(track, local_video)))

    await pc.setRemoteDescription(offer)
    answer = await pc.createAnswer()
    await pc.setLocalDescription(answer)

    return web.Response(
        content_type='application/json',
        text=json.dumps({
            'sdp': pc.localDescription.sdp,
            'type': pc.localDescription.type
        }))


pcs = []

async def on_shutdown(app):
    # stop audio / video consumers
    for pc in pcs:
        for c in pc._consumers:
            c.cancel()

    # close peer connections
    coros = [pc.close() for pc in pcs]
    await asyncio.gather(*coros)

    # --- for worker process --
    executor.shutdown()

FLAGS = []
# --- start web app ---
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    
    parser.add_argument('-m', '--model-path',
		type=str,
		default='./yolov3-coco/',
		help='The directory where the model weights and \
			  configuration files are.')
    
    parser.add_argument('-w', '--weights',
		type=str,
		default='./yolov3-coco/yolov3.weights',
		help='Path to the file which contains the weights \
			 	for YOLOv3.')
    
    parser.add_argument('-cfg', '--config',
		type=str,
		default='./yolov3-coco/yolov3.cfg',
		help='Path to the configuration file for the YOLOv3 model.')
    
    parser.add_argument('-l', '--labels',
		type=str,
		default='./yolov3-coco/coco-labels',
		help='Path to the file having the \
					labels in a new-line seperated way.')
                    
    parser.add_argument('-c', '--confidence',
		type=float,
		default=0.5,
		help='The model will reject boundaries which has a \
				probabiity less than the confidence value. \
				default: 0.5')
    
    parser.add_argument('-th', '--threshold',
		type=float,
		default=0.3,
		help='The threshold to use when applying the \
				Non-Max Suppresion')
                
    parser.add_argument('--download-model',
		type=bool,
		default=False,
		help='Set to True, if the model weights and configurations \
				are not present on your local machine.')
                
    parser.add_argument('-t', '--show-time',
		type=bool,
		default=True,
		help='Show the time taken to infer each image.')

    FLAGS, unparsed = parser.parse_known_args()

	# Get the labels
    labels = open(FLAGS.labels).read().strip().split('\n')

	# Intializing colors to represent each label uniquely
    colors = np.random.randint(0, 255, size=(len(labels), 3), dtype='uint8')

	# Load the weights and configutation to form the pretrained YOLOv3 model
    net = cv.dnn.readNetFromDarknet(FLAGS.config, FLAGS.weights)

	# Get the output layer names of the model
    layer_names = net.getLayerNames()
    layer_names = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
    
    parser = argparse.ArgumentParser(description='WebRTC audio / video / data-channels demo')
    parser.add_argument('--port', type=int, default=8080,
                        help='Port for HTTP server (default: 8080)')
    parser.add_argument('--verbose', '-v', action='count')
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)

    # --- executor for worker process --
    # WARN; currentry for 1 PeerConnection
    #executor = ProcessPoolExecutor(1)
    executor = ThreadPoolExecutor(1)


    # --- web app --
    app = web.Application()
    app.on_shutdown.append(on_shutdown)
    app.router.add_get('/', index)
    app.router.add_get('/client.js', javascript)
    app.router.add_post('/offer', offer)
    web.run_app(app, port=args.port)